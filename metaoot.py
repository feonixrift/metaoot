#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
 metaoot.py

 Metadata manager for Tooting on Mastodon.
 CLI mode should include raw conversation.

 author: Kim Reece <kim.reece@protonmail.com>
                    @feonixrift@hackers.town

 last modified: June 2020
"""

import os
import shutil
import json
import datetime
from mastodon import Mastodon, MastodonUnauthorizedError

SCOPE = [
        "read:statuses",
        "read:accounts",
        "read:follows",
        "write:statuses",
        "read:notifications",
        "write:accounts"
    ]

BASE_PATH = os.path.expanduser("~/.metaoot/")

def urlize(s):
    """ Turn handy directory name back into a server url """
    return "https://"+s+"/"
    
def custom_serializer(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()

def jprint(d):
    print(json.dumps(d, indent=2, default=custom_serializer))

# In the rare case that a crash left the tree inconsistent with the keys,
# or if some keys were removed to be regenerated, this will oauth and
# negotiate all required secrets. Otherwise it just logs in everywhere.
# TODO Separate pulling this information in from doing network actions.
m = []
servers = next(os.walk(BASE_PATH))[1]
for s in servers:
    s_path = os.path.join(BASE_PATH, s)
    fs = os.path.join(s_path, "clientcred.secret")
    if not os.path.exists(fs):
        Mastodon.create_app(
            "metaoot",
            api_base_url=urlize(s),
            to_file=fs,
            scopes=SCOPE,
            website="https://gitlab.com/feonixrift/metaoot")
    accounts = next(os.walk(s_path))[1]
    for a in accounts:
        a_path = os.path.join(s_path, a)
        fa = os.path.join(a_path, "usercred.secret")
        if not os.path.exists(fa):
            c = Mastodon(
                client_id=fs,
                api_base_url=urlize(s))
            cu = c.auth_request_url(scopes=SCOPE)
            print("Open this: {}".format(cu))
            ci = input("Secret is: ")
            c.log_in(code=ci, scopes=SCOPE, to_file=fa)
        c = Mastodon(
            client_id=fs,
            access_token=fa,
            api_base_url=urlize(s),
#            ratelimit_method="pace",
#            ratelimit_pacefactor=0.8)
            ratelimit_method="wait")
        try:
            ca = c.account_verify_credentials()
            m.append({"server": s, "account": a, "api": c, "me": ca})
        except MastodonUnauthorizedError:
            print("Deactivate: {} at {}".format(a, s))

# Users on the same server as requester are sent merely as usernames,
# and must have the associated server appended to be consistent.
def acct_normalize(an, s):
    """ Normalize account names for server non-specific context """
    return an if "@" in an else an+"@"+s

# This is the normal account generation flow. For convenience it also
# performs an initial login.
# TODO Make the login a user choice.
def mm_add():
    """ Add an account """
    s = input("Server: ")
    s_path = os.path.join(BASE_PATH, s)
    fs = os.path.join(s_path, "clientcred.secret")
    if not os.path.exists(s_path):
        os.mkdir(s_path)
        Mastodon.create_app(
            "metaoot",
            api_base_url=urlize(s),
            to_file=fs,
            scopes=SCOPE,
            website="https://gitlab.com/feonixrift/metaoot")
    a = input("Account: ")
    a_path = os.path.join(s_path, a)
    if not os.path.exists(a_path):
        fa = os.path.join(a_path, "usercred.secret")
        os.mkdir(a_path)
        c = Mastodon(
            client_id=fs,
            api_base_url=urlize(s))
        cu = c.auth_request_url(scopes=SCOPE)
        print("Open this: {}".format(cu))
        ci = input("Secret is: ")
        c.log_in(code=ci, scopes=SCOPE, to_file=fa)
        c = Mastodon(
            client_id=fs,
            access_token=fa,
            api_base_url=urlize(s),
            ratelimit_method="pace",
            ratelimit_pacefactor=0.8)
#            ratelimit_method="wait")
        try:
            ca = c.account_verify_credentials()
            m.append({"server": s, "account": a, "api": c, "me": ca})
        except MastodonUnauthorizedError:
            print("Deactivate: {} at {}".format(a, s))

def mm_list():
    """ List accounts """
#    jprint([[i, a["account"]+"@"+a["server"]] for i,a in enumerate(m)])
    l = [[i,a["account"]+"@"+a["server"]] for i,a in enumerate(m)]
    for ll in l:
        print(ll[0], ': ', ll[1])

def mm_del():
    """ Delete account from local list by index """
    mi = select_account()
    s_path = os.path.join(BASE_PATH, mi["server"])
    a_path = os.path.join(s_path, mi["account"])
    shutil.rmtree(a_path)
    if not next(os.walk(BASE_PATH))[1]:
        shutil.rmtree(s_path)

def mm_save():
    """ Save meta """
    acct = select_account()
    c = acct["api"]
    me = acct["me"]
    s_path = os.path.join(BASE_PATH, acct["server"])
    a_path = os.path.join(s_path, acct["account"])
    d = datetime.date.today().strftime("%Y-%m-%d")
    d_path = os.path.join(a_path, d)
    f_path = os.path.join(d_path, "follows.json")
    if os.path.exists(f_path):
        print(""" File already exists. Move, remove, or otherwise handle and then try again. """)
    if not os.path.exists(d_path):
        os.mkdir(d_path)

    # follows
    f = c.account_following(me)
    while hasattr(f[-1], '_pagination_next'):
        f.extend(c.account_following(me, max_id=f[-1]._pagination_next['max_id']))
        print('.', end='', flush=True)

    # pace of the follows
    # currently using just one page to estimate
    follows = [{
        "id": a.id,
        "account": acct_normalize(a.acct, acct["server"]),
        "pace": -1,
        } for a in f]
    n = datetime.datetime.now(datetime.timezone.utc)
    for a in follows:
        t = c.account_statuses(a["id"])
        if t:
            td = (n - t[-1].created_at).total_seconds()
            a["pace"] = 1000
            if (td > 17280):
                a["pace"] = 100
            if (td > 172800):
                a["pace"] = 10
            if (td > 1728000):
                a["pace"] = 1

    # save out data
    with open(f_path, "w+") as file:
        json.dump(follows, file, indent=2)

def mm_res():
    """ Restore meta """
    print("mm_res")

def mm_clean():
    """ Clean meta """
    print("mm_clean")

def mm_insp():
    """ Inspect meta """
    mi = select_account()
    c = mi["api"]
    me = mi["me"]
    f = c.account_following(me)
    while hasattr(f[-1], '_pagination_next'):
        f.extend(c.account_following(me, max_id=f[-1]._pagination_next['max_id']))
        print('.', end='', flush=True)
    jprint([acct_normalize(a.acct, mi["server"]) for a in f])

def select_account():
    """ Select which account from the list to act on """
    mm_list()
    i = int(input("Index: "))
    return m[i]

def mm_archive():
    """ Archive all toots """
    mi = select_account()
    c = mi["api"]
    me = mi["me"]
    s_path = os.path.join(BASE_PATH, mi["server"])
    a_path = os.path.join(s_path, mi["account"])
    d = datetime.date.today().strftime("%Y-%m-%d")
    d_path = os.path.join(a_path, d)
    f_path = os.path.join(d_path, "statuses.json")
    if os.path.exists(f_path):
        print(""" You already did something today? This may overwrite. Clarify your intentions by deleting or moving the file manually, then begin again. """)
        return
    if not os.path.exists(d_path):
        os.mkdir(d_path)

    print("Status count:", me["statuses_count"])
    t = c.account_statuses(me)
    i = 0
    while hasattr(t[-1], '_pagination_next'):
        t.extend(c.account_statuses(me, max_id=t[-1]._pagination_next['max_id']))
        print('.', end='', flush=True)
        i = i + 1
        if i % 10 == 0:
            print(' ')

    with open(os.path.join(d_path, "statuses.json"), "w+") as file:
        json.dump(t, file, indent=2, default=custom_serializer)

    print("saved.")

def mm_gut():
    """ Gut account -- delete old toots """
    mi = select_account()
    c = mi["api"]
    me = mi["me"]
    s_path = os.path.join(BASE_PATH, mi["server"])
    a_path = os.path.join(s_path, mi["account"])
    d = datetime.date.today().strftime("%Y-%m-%d")
    d_path = os.path.join(a_path, d)
    f_path = os.path.join(d_path, "statuses.json")
    if not os.path.exists(f_path):
        print(""" There is definitely no backup for today, please do that first. You may either make a new backup or copy an older one into the current day's directory. Only toots contained in a backup file will be considered for deletion.""")
        return

    n = datetime.datetime.now(datetime.timezone.utc)
    with open(f_path, "r") as f:
        t = json.load(f)
        for s in t:
            td = (n - datetime.datetime.strptime(s["created_at"], "%Y-%m-%d %H:%M:%S.%f%z")).days
            keep = False
            if td < 365:
                keep = True
            if "bookmarked" in s and s["bookmarked"]:
                keep = True
            if "pinned" in s and s["pinned"]:
                keep = True
            if not keep:
                try:
                    c.status_delete(s["id"])
                    print(".", end='', flush=True)
                except:
                    print("_", end='', flush=True)
            else:
                print("`", end='', flush=True)
        print("")

def mm_quit():
    """ Quit """
    exit(0)

def banner():
    print("""
    Welcome to metaoot. Yes the UI is awful; see also work in progress.

    This software is unstable and may destroy local data you didn't tell it to.
    Back your stuff up.

    This software should only destroy remote data when you tell it to.
    I make no guarantees.
    Back your stuff up.

    Check your backups. I'll wait.

    The rate limit method in use is PACE.
    This means every single paginated server request takes a second.
    Expect tedium.
    """)

CALL = {
    'a': mm_add,
    'l': mm_list,
    'd': mm_del,
    's': mm_save,
    'r': mm_res,
    'c': mm_clean,
    'i': mm_insp,
    'h': mm_archive,
    'g': mm_gut,
    'q': mm_quit}

if __name__ == "__main__":
    banner()
    while True:
        print("""metaoot main menu
a) Add an account to list
l) List accounts
d) Delete an account from list
s) Save meta
r) Restore meta (disabled)
c) Clean meta (disabled)
i) Inspect
h) Archive toots (unofficial)
g) Gut account (delete old toots)
q) Quit
""")
    # default to l since it can't hurt anything
        CALL.get(input(">> "), mm_list)()
